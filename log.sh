#!/bin/bash
PAR_LOGFILE="$1"
DEBUG_MODE="$2"

function log
{
    local logtype=$1
    local msg=$2

    datetime=`date +'%F %H:%M:%S'`
    if [ "${DEBUG_MODE}" = "debug" ];then
        logformat="[${logtype} ${datetime} $0:`caller 0 | awk '{print$1}'`] ${msg}"
    else
        logformat="[${logtype} ${datetime} ${msg}"
    fi

    if [ "${PAR_LOGFILE}" = "" ];then
        echo -e "${logformat}"
    else
        echo -e "${logformat}" | tee -a ${PAR_LOGFILE}
    fi
}