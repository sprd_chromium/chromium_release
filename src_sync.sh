#!/bin/bash
# **************************************************************
# *  Filename:    src_syc.sh   
# *  Copyright:   Spreadtrum Co., Ltd.
# *  @author:     jiawang.yu@unisoc.com
# *  @version     2019/1/10  @Reviser  Initial Version
# **************************************************************
PAR_CHROMIUM_HOME=$1
PAR_BRANCH_DIR=$2
PAR_LOG_FILE=$3
PAR_BRANCH_NAME=${PAR_BRANCH_DIR##*/}

CHROMIUM_REPOSITORY="gitosis@10.0.0.160:chromium/chromium/tools/depot_tools.git"

function src_sync()
{
    cd ${PAR_BRANCH_DIR}
    log info "start sync "${PAR_BRANCH_NAME}" source"

    num_of_dir=`ls -l |wc -l`
    if [ "`ls -A ${PAR_BRANCH_DIR}`" = "" ] || [ ${num_of_dir} -lt 15 ];then
        log info "${PAR_BRANCH_DIR} is a damaged chromium source directory, re sync init."
        rm -rf ${PAR_BRANCH_DIR}
        mkdir -p ${PAR_BRANCH_DIR}
        cd ${PAR_BRANCH_DIR}

        git clone -b ${PAR_BRANCH_NAME} ${CHROMIUM_REPOSITORY}
        export PATH=${PAR_BRANCH_DIR}/depot_tools:"$PATH"
        fetch android

        cd src
        gclient sync
    else
        git clean -dfx >>${LOG_FILE} 2>&1
        git reset --hard HEAD >>${LOG_FILE} 2>&1
        git fetch >>${LOG_FILE} 2>&1
        git rebase origin >>${LOG_FILE} 2>&1
    fi
    log info "${PAR_BRANCH_NAME} src sync finished."
}

function init_log()
{
    local log_file="$1"
    local log_level="$2"

    source "${PAR_CHROMIUM_HOME}/bin/log.sh" "${log_file}" "${log_level}"
}

function main()
{
    init_log "${PAR_LOG_FILE}/" "debug"

    log info "=================== src_sync.sh main info =================================="
    log info "PAR_BRANCH_DIR:${PAR_BRANCH_DIR}"
    log info "PAR_LOG_FILE:${PAR_LOG_FILE}"
    log info "PAR_BRANCH_NAME:${PAR_BRANCH_NAME}"
    log info "detail >> ${PAR_LOG_FILE}"

    if [ ! -d "${PAR_BRANCH_DIR}" ];then
        mkdir ${PAR_BRANCH_DIR}
        if [ $? -ne 0 ];then
           log error "mkdir ${PAR_BRANCH_DIR} faild."
           exit 1
        fi
        log info "mkdir ${PAR_BRANCH_DIR} success."
    fi

    src_sync
}

main