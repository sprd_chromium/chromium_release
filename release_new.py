#/usr/bin/python
""" Release newest version webview.apks to the selected android code path.
    This script is for sprd_chromium_53 or later.
"""

import os,re,fnmatch,sys,shutil,time
import subprocess
import traceback
import tempfile
import ftplib
from ftplib import FTP
import logging

# We use GN build the release version.

# Set the release dir.
ARM_DIR   = "out/arm_release/"
ARM64_DIR = "out/arm64_release/"
X86_DIR   = "out/x86_release/"
X64_DIR   = "out/x64_release/"

# We have already set the GN`s args in their release dirs, which are as below:
ARM_RELEASE_PATH   = SRC_PATH + ARM_DIR
ARM64_RELEASE_PATH = SRC_PATH + ARM64_DIR
X86_RELEASE_PATH   = SRC_PATH + X86_DIR
X64_RELEASE_PATH   = SRC_PATH + X64_DIR

# Thus we all know that if we have not clear the out dir, the version may be incorrect.
# In case of it, we should clear them first.
ARM_OUT_DIR_PATH = SRC_PATH + ARM_DIR +"system_webview_apk/"
ARM64_OUT_DIR_PATH = SRC_PATH + ARM64_DIR +"system_webview_apk/"
X86_OUT_DIR_PATH = SRC_PATH + X86_DIR + "system_webview_apk/"
X64_OUT_DIR_PATH = SRC_PATH + X64_DIR + "system_webview_apk/"

# The target branch name
BRANCH_NAME = "sprd_chromium_59.0.3048.0"

# The depot_tools is out of the src dir. We must assign it and export it to the PATH.
DEPOT_TOOLS_PATH = "/home8/qiuya.wang/chromium_59/depot_tools"

# Store the symbols.
SYMBOLS_STORE_PATH = "/home8/qiuya.wang/build_symbols/59/"

# The sprd-webview git repository.
#SPRD_WEBVIEW_GIT_PATH = "/home8/qiuya.wang/sprd-webview-repository/M59/sprd-webview"
SPRD_WEBVIEW_GIT_PATH = "/home8/qiuya.wang/sprdroid8.1_trunk/vendor/sprd/sprd-webview/"


def make_and_copy():

    # First try to rebase the src code.
    try:
        cmd = "cd " + SRC_PATH + " && git fetch && git checkout -- ./ && git rebase origin/" + BRANCH_NAME
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mGit Rebase origin/" + BRANCH_NAME + ".\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()

    # GN generater step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && gn gen out/arm_release && gn gen out/arm64_release && gn gen out/x86_release && gn gen out/x64_release"
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mRe-gen the release folder.\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()

    #Build arm step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/arm_release system_webview_apk"
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the arm release version. Please wait.\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()

    #Build arm64 step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/arm64_release system_webview_apk"
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the arm64 release version. Please wait.\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()
    # Needn`t repack after M59.
    try:
        cmd = "cp " + ARM_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/arm/webview.apk && " \
            + "cp " + ARM64_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/arm64/webview.apk"
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mMoving arm and arm64 webview.apk, please wait!\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()

    #Build x86 step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/x86_release system_webview_apk"
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the x86 release version. Please wait.\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()

    #Build arm64 step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/x64_release system_webview_apk"
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the x64 release version. Please wait.\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()

    # Needn`t repack after M59.
    try:
        cmd = "cp " + X86_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/x86/webview.apk && " \
            + "cp " + X64_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/x86_64/webview.apk"
        out_temp = tempfile.SpooledTemporaryFile(bufsize=10*10000)
        fileno = out_temp.fileno()
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mMoving x86 and x64 webview.apk, please wait!\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()


    dir_name = SYMBOLS_STORE_PATH + get_package_version()
    arm_dir_name = dir_name + "/arm"
    arm64_dir_name = dir_name + "/arm64"
    x86_dir_name = dir_name + "/x86"
    x64_dir_name = dir_name + "/x64"

    if os.path.isdir(dir_name) == False:
        os.mkdir(dir_name)
    if os.path.isdir(arm_dir_name) == False:
        os.mkdir(arm_dir_name)
    if os.path.isdir(arm64_dir_name) == False:
        os.mkdir(arm64_dir_name)
    if os.path.isdir(x86_dir_name) == False:
        os.mkdir(x86_dir_name)
    if os.path.isdir(x64_dir_name) == False:
        os.mkdir(x64_dir_name)
    print "Start copy symbol files."
    shutil.copyfile(ARM_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", arm_dir_name + "/libwebviewchromium.so")
    shutil.copyfile(ARM64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", arm64_dir_name + "/libwebviewchromium.so")
    shutil.copyfile(X86_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", x86_dir_name + "/libwebviewchromium.so")
    shutil.copyfile(X64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", x64_dir_name + "/libwebviewchromium.so")
    # Save the symbols lib to the ftp.
    ftp = FTP()
    timeout = 30
    port = 21
    ftp.connect('10.0.6.4',port,timeout)
    ftp.login('tianqi','6ZS675Ql')
    print ftp.getwelcome()
    ftp.cwd('/chromium_symbols/59')
    ftp_dir_name = get_package_version()
    ftp_dir_list = ftp.dir()
    try:
        ftp.cwd(ftp_dir_name)
    except ftplib.error_perm:
        try:
            ftp.mkd(ftp_dir_name)
            ftp.cwd(ftp_dir_name)
            ftp.mkd("arm")
            ftp.mkd("arm64")
            ftp.mkd("x86")
            ftp.mkd("x64")
        except ftplib.error_perm:
            print "\033[1;31;40myou have no authority to make dir"
    finally:
        fp = open(ARM_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store arm edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/arm/libwebviewchromium.so" , fp, 1024)

        fp = open(ARM64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store arm64 edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/arm64/libwebviewchromium.so" , fp, 1024)

        fp = open(X86_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store x86 edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/x86/libwebviewchromium.so" , fp, 1024)

        fp = open(X64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store x64 edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/x64/libwebviewchromium.so" , fp, 1024)

        ftp.quit()

    print "\033[1;31;40mStore done.\033[0m"

result = make_and_copy()



# TODO: We could use built-in version tag.
def get_package_version():
    version_number = time.strftime("%y.%W")
    day = time.strftime("%w")
    temp_version_float = float(version_number)
    year = int(temp_version_float)
    week = int(temp_version_float * 100 - float(year) * 100)
    if day != 0:
        week = week + 1
    else:
        day = 7

    version_number = str(year) + '.' + str(week) + '.' + str(day)
    print version_number
    return version_number

def deploy()
    dir_name = SYMBOLS_STORE_PATH + get_package_version()
    arm_dir_name = dir_name + "/arm"
    arm64_dir_name = dir_name + "/arm64"
    x86_dir_name = dir_name + "/x86"
    x64_dir_name = dir_name + "/x64"

    if os.path.isdir(dir_name) == False:
        os.mkdir(dir_name)
    if os.path.isdir(arm_dir_name) == False:
        os.mkdir(arm_dir_name)
    if os.path.isdir(arm64_dir_name) == False:
        os.mkdir(arm64_dir_name)
    if os.path.isdir(x86_dir_name) == False:
        os.mkdir(x86_dir_name)
    if os.path.isdir(x64_dir_name) == False:
        os.mkdir(x64_dir_name)
    print "Start copy symbol files."
    shutil.copyfile(ARM_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", arm_dir_name + "/libwebviewchromium.so")
    shutil.copyfile(ARM64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", arm64_dir_name + "/libwebviewchromium.so")
    shutil.copyfile(X86_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", x86_dir_name + "/libwebviewchromium.so")
    shutil.copyfile(X64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", x64_dir_name + "/libwebviewchromium.so")
    # Save the symbols lib to the ftp.
    ftp = FTP()
    timeout = 30
    port = 21
    ftp.connect('10.0.6.4',port,timeout)
    ftp.login('tianqi','6ZS675Ql')
    print ftp.getwelcome()
    ftp.cwd('/chromium_symbols/59')
    ftp_dir_name = get_package_version()
    ftp_dir_list = ftp.dir()
    try:
        ftp.cwd(ftp_dir_name)
    except ftplib.error_perm:
        try:
            ftp.mkd(ftp_dir_name)
            ftp.cwd(ftp_dir_name)
            ftp.mkd("arm")
            ftp.mkd("arm64")
            ftp.mkd("x86")
            ftp.mkd("x64")
        except ftplib.error_perm:
            print "\033[1;31;40myou have no authority to make dir"
    finally:
        fp = open(ARM_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store arm edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/arm/libwebviewchromium.so" , fp, 1024)

        fp = open(ARM64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store arm64 edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/arm64/libwebviewchromium.so" , fp, 1024)

        fp = open(X86_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store x86 edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/x86/libwebviewchromium.so" , fp, 1024)

        fp = open(X64_RELEASE_PATH + "lib.unstripped/libwebviewchromium.so", 'rb')
        print "\033[1;31;40mStart to store x64 edition in ftp.\033[0m"
        ftp.storbinary('STOR '+ '/chromium_symbols/59/' + ftp_dir_name + "/x64/libwebviewchromium.so" , fp, 1024)

        ftp.quit()

    print "\033[1;31;40mStore done.\033[0m"



def unpack()
    

def build()
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && gn gen out/arm_release && gn gen out/arm64_release && gn gen out/x86_release && gn gen out/x64_release"

        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mRe-gen the release folder.\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()

    #Build arm step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/arm_release system_webview_apk"
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the arm release version. Please wait.\033[0m"
        obj.wait()

    except Exception, e:
        print traceback.format_exc()

    #Build arm64 step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/arm64_release system_webview_apk"
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the arm64 release version. Please wait.\033[0m"
        obj.wait()
    except Exception, e:
        print traceback.format_exc()

    # Needn`t repack after M59.
    try:
        cmd = "cp " + ARM_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/arm/webview.apk && " \
            + "cp " + ARM64_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/arm64/webview.apk"

        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mMoving arm and arm64 webview.apk, please wait!\033[0m"
        obj.wait()
        out_temp.seek(0)
    except Exception, e:
        print traceback.format_exc()

    #Build x86 step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/x86_release system_webview_apk"
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the x86 release version. Please wait.\033[0m"
        obj.wait()
    except Exception, e:
        print traceback.format_exc()

    #Build arm64 step.
    try:
        cmd = "export PATH=" + DEPOT_TOOLS_PATH + ":\"$PATH\" && cd " + SRC_PATH + " && ninja -C out/x64_release system_webview_apk"

        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mBuild the x64 release version. Please wait.\033[0m"
        obj.wait()
    except Exception, e:
        print traceback.format_exc()

    # Needn`t repack after M59.
    try:
        cmd = "cp " + X86_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/x86/webview.apk && " \
            + "cp " + X64_RELEASE_PATH + "/apks/SystemWebView.apk " + SPRD_WEBVIEW_GIT_PATH + "/prebuilt/x86_64/webview.apk"

        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mMoving x86 and x64 webview.apk, please wait!\033[0m"
        obj.wait()
    except Exception, e:
        print traceback.format_exc()
    finally:
        if out_temp:
            out_temp.close()

def sync_src(branch_dir, branch)
    try:
        cmd = "cd " + branch_dir + " && git fetch && git checkout -- ./ && git rebase origin/" + branch
        obj = subprocess.Popen(cmd,stdout=None,stderr=None,shell=True)
        print "\033[1;31;40mGit Rebase origin/" + BRANCH_NAME + ".\033[0m"
        obj.wait()
    except Exception, e:
        print traceback.format_exc()

def initLogger(branch_out_dir, branch):
    if (False == os.path.exists(branch_out_dir)):
        os.makedirs(branch_out_dir)
    
    date = time.strftime("%y-%m-%d_%H%M")
    logFile = branch+'_release_'+date+'.log'

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    fh = logging.FileHandler(logFile, mode='w')  
    fh.setLevel(logging.INFO)
 
    ch = logging.StreamHandler()  
    ch.setLevel(logging.INFO)

    formatter = logging.Formatter("[%(asctime)s %(funcName)s %(filename)s:%(lineno)d - %(levelname)s] %(message)s")
    fh.setFormatter(formatter)  
    ch.setFormatter(formatter)  

    logger.addHandler(fh)  
    logger.addHandler(ch)

def main():
    parser = argparse.ArgumentParser()
  
    parser.add_argument('branch')
    parser.add_argument('-c', '--target_cpu', default='arm64')
    parser.add_argument('-w', '--work_dir', default='/home/newDisk2/apuser/chromium')

    args = parser.parse_args()
    
    branch = args.branch
    
    work_dir   = args.work_dir
    if ('/' != work_dir[len(work_dir)-1]):
        work_dir += '/'

    branch_dir = work_dir + branch + '/'

    target_cpu = args.target_cpu
    branch_out_dir = branch_dir + 'out/' +  target_cpu + ''
    initLogger(branch_out_dir, branch)

    # sync src
    sync_src(branch_dir, branch)

if __name__ == '__main__':
    main()