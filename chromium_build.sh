#!/bin/bash
# **************************************************************
# *  Filename:    build.sh   
# *  Copyright:   Spreadtrum Co., Ltd.
# *  @author:     jiawang.yu@unisoc.com
# *  @version     2019/1/10  @Reviser  Initial Version
# **************************************************************
if [ ! -n "$1" ] ;then
    echo "input the correct chromium home."
    exit 1
elif [ ! -n "$2" ] ;then
    echo "input the correct code path please."
    exit 1
elif [ ! -n "$3" ] ;then
    echo "input product name please."
    exit 1
fi

PAR_CHROMIUM_HOME=$1
PAR_BRANCH_BASE_DIR=$2
PAR_TARGET=$3
PAR_BRANCH_OUT_DIR=$4

function do_build()
{
    cd ${PAR_BRANCH_BASE_DIR}/src
    log info "at $(pwd) build ${PAR_TARGET} ${PAR_MODULE} > ${LOG_FILE}"

    gn gen out/${PAR_TARGET} "--args=target_os=\"android\" target_cpu=\"arm\" is_debug=false symbol_level=2 is_official_build=true is_chrome_branded=false use_official_google_api_keys=false enable_resource_whitelist_generation=true ffmpeg_branding=\"Chrome\" proprietary_codecs=true enable_remoting=true ignore_elf32_limitations=true"
    ninja -C out/${PAR_TARGET} system_webview_apk

    return 0
}

function setup_host_env()
{
    export PATH=${PAR_BRANCH_BASE_DIR}/depot_tools:"$PATH"
}

function init_log()
{
    local log_dir="$1"
    local log_level="$2"

    if [ ! -d "${log_dir}" ]; then 
      mkdir -p ${log_dir}
    fi

    local run_date=`date "+%y-%m-%d_%H%M%S"`
    LOG_FILE="${log_dir}/chromium_build_${run_date}.log"

    source "${PAR_CHROMIUM_HOME}/bin/log.sh" "${LOG_FILE}" "${log_level}"
}

function main()
{
    if [ "/" = "${PAR_BRANCH_BASE_DIR: -1}" ];then
        PAR_BRANCH_BASE_DIR=${PAR_BRANCH_BASE_DIR%?}
    fi

    if [ ! -d "${PAR_BRANCH_BASE_DIR}" ] || [ "`ls -A ${PAR_BRANCH_BASE_DIR}`" = "" ];then
        log error "The directory ${PAR_BRANCH_BASE_DIR} does not exist or the directory is empty!"
        exit 1
    fi

    init_log "${PAR_BRANCH_OUT_DIR}" "debug"
    log info "=================== build.sh main info ====="
    log info "PAR_BRANCH_BASE_DIR:${PAR_BRANCH_BASE_DIR}"
    log info "PAR_TARGET:${PAR_TARGET}"

    setup_host_env "${PAR_BRANCH_BASE_DIR}"

    do_build
}

main
