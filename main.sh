#!/bin/bash
# **************************************************************
# *  Filename:    cov_run.sh   
# *  Copyright:   Spreadtrum Co., Ltd.
# *  @author:     jiawang.yu@unisoc.com
# *  @version     2019/1/10  @Reviser  Initial Version
# **************************************************************
if [ ! -n "$1" ] ;then
    echo "input the correct android branch name."
    exit 1
elif [ ! -n "$2" ] ;then
    echo "input the correct chromium branch name."
    exit 1
fi

PAR_ANDROID_BRANCH=$1
PAR_CHROMIUM_BRANCH=$2
PAR_SYNC_SRC="false"

CHROMIUM_HOME="/home/chromium"

BRANCH_BASE_DIR="${CHROMIUM_HOME}/src/${PAR_CHROMIUM_BRANCH}"
SPRD_WEBVIEW_DIR=${CHROMIUM_HOME}/src/sprd-webview/

BUILD_SHELL="${CHROMIUM_HOME}/bin/chromium_build.sh"
SRC_SYNC_SHELL="${CHROMIUM_HOME}/bin/src_sync.sh"
MAIL_PY="python ${CHROMIUM_HOME}/bin/sendmail.py"

function init_log()
{
    local log_dir="$1"
    local log_level="$2"

    if [ ! -d "${log_dir}" ]; then 
      mkdir -p ${log_dir}
    fi

    local run_date=`date "+%y-%m-%d_%H%M%S"`
    LOG_FILE="${log_dir}/main_${run_date}.log"

    source "${CHROMIUM_HOME}/bin/log.sh" "${LOG_FILE}" "${log_level}"
}

function webview_push()
{
    local branch="$1"
    log info "4. webview_push git push ssh://jiawang.yu@10.0.0.160:29418/chromium/chromium/src HEAD:refs/for/${branch} "
    git commit -a -m "update system webview apk"
    git push ssh://jiawang.yu@10.0.0.160:29418/chromium/chromium/src HEAD:refs/for/${branch}
}

function update_webview_apk()
{
    local source_apk="$1"
    local target="$2"
    log info "3. update ${target} webview apk ... "

    cd "${CHROMIUM_HOME}/src"
    if [ "`ls -A ${SPRD_WEBVIEW_DIR}`" = "" ]; then
        git clone ssh://jiawang.yu@review.source.unisoc.com:29418/vendor/sprd/sprd-webview
    fi
    git checkout origin/${PAR_ANDROID_BRANCH}
    cp ${source_apk} ${SPRD_WEBVIEW_DIR}/prebuilt/${target}/
}

function main()
{
    cd "${CHROMIUM_HOME}/bin"

    #TARGETS="arm arm64 x86 x86_64"
    TARGETS="arm arm64"
    for TARGET in ${TARGETS// / }
    do
        local TARGET_OUT_DIR="${BRANCH_BASE_DIR}/src/out/${TARGET}"

        init_log ${TARGET_OUT_DIR} "debug"
        log info "=================== chromium main.sh main info =================================="
        log info "PAR_ANDROID_BRANCH:${PAR_ANDROID_BRANCH}"
        log info "PAR_CHROMIUM_BRANCH:${PAR_CHROMIUM_BRANCH}"
        log info "PAR_SYNC_SRC:${PAR_SYNC_SRC}"

        # 1. src sync
        log info "1. ${PAR_CHROMIUM_BRANCH} src sync ... "
        if [ "`ls -A ${BRANCH_BASE_DIR}`" = "" ]; then
            ${SRC_SYNC_SHELL} ${CHROMIUM_HOME} ${BRANCH_BASE_DIR} ${LOG_FILE} 2>&1 | tee -a ${LOG_FILE}
            if [ ${PIPESTATUS[0]} -ne 0 ]; then
                ${MAIL_PY} ${PAR_CHROMIUM_BRANCH} -m "${BRANCH_BASE_DIR} ${CHROMIUM_HOME} ${TARGET} ${PAR_SYNC_SRC} src sync error"
                log error "src sync failed!! detail >> ${TARGET_OUT_DIR}"
                return 1
            fi
        fi

        # 2. build
        log info "2. ${PAR_CHROMIUM_BRANCH}:${TARGET} build "
        ${BUILD_SHELL} "${CHROMIUM_HOME}" "${BRANCH_BASE_DIR}" "${TARGET}" "${TARGET_OUT_DIR}" 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            ${MAIL_PY} ${PAR_CHROMIUM_BRANCH} -m "${BRANCH_BASE_DIR} ${CHROMIUM_HOME} ${TARGET} ${PAR_SYNC_SRC} build failed."
            log error "${PAR_CHROMIUM_BRANCH}:${TARGET} build failed!! detail >> ${TARGET_OUT_DIR}"
            return 1
        fi

        # 3. update webview apk
        update_webview_apk ${TARGET_OUT_DIR}/apks/SystemWebView.apk ${TARGET}
    done

    # 4. webview push
    webview_push ${PAR_CHROMIUM_BRANCH}

    log info "${PAR_CHROMIUM_BRANCH} release success."
}

main
