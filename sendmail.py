# encoding: utf-8  
#!/usr/bin/python
# **************************************************************
# *  Filename:    sendmail.py   
# *  Copyright:   Spreadtrum Co., Ltd.
# *  @author:     jiawang.yu@unisoc.com
# *  @version     2019/1/10  @Reviser  Initial Version
# **************************************************************
import sys
import os

#mail
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib

import argparse

def sendMail(to_list, project, comment, attach):

    all_list = []

    msg = MIMEMultipart('alternative')
    msg.attach(MIMEText(comment,'plain','utf-8'))

    try:
        att1 = MIMEText(open(attach, 'rb').read(), 'base64', 'utf-8')
        att1["Content-Type"] = 'application/octet-stream'
        att1["Content-Disposition"] = 'attachment; filename=log.txt'
        msg.attach(att1)
    except Exception, why:
        print "Add attachments1 Exception:%s"%why

    #add mail title and mail list
    mail_host = "mail.unisoc.com"
    mail_user = "psaadmin"
    mail_postfix = "unisoc.com"

    msg['To'] = ",".join(to_list)
    msg['From'] = mail_user+'@unisoc.com'
    msg['Subject'] = project + ' at 10.0.35.49 chromium release error'

    for to in to_list:
        all_list.append(to)

    #send
    try:
        server = smtplib.SMTP(mail_host,25)
        server.set_debuglevel(0)
        server.starttls()
        server.sendmail(msg['from'], all_list, msg.as_string())
        server.quit()
        print 'send successfully!'
    except Exception, e:
        print(e)

def main():
    parser = argparse.ArgumentParser()
  
    parser.add_argument('project')
    parser.add_argument('-m', '--comment', default='')
    parser.add_argument('-t', '--to', default='')
    parser.add_argument('-a', '--attach', default='')

    args = parser.parse_args()
    to_list = ['jiawang.yu','xiang.qiu','linyu.zhou','qiuya.wang','cheng.du']
    
    sendMail(to_list, args.project, args.comment, args.attach)

if __name__ == '__main__':
    main()
