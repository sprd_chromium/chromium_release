#!/bin/bash
# **************************************************************
# *  Filename:    build.sh   
# *  Copyright:   Spreadtrum Co., Ltd.
# *  @author:     jiawang.yu@unisoc.com
# *  @version     2019/1/10  @Reviser  Initial Version
# **************************************************************
if [ ! -n "$1" ] ;then
    echo "input the correct code path please."
    exit 1
elif [ ! -n "$2" ] ;then
    echo "input product name please."
    exit 1
elif [ ! -n "$3" ] ;then
    echo "input module name please."
    exit 1
fi

PAR_SRC_DIR="$1"
PAR_PRODUCT="$2"
PAR_MODULE="$3"
PAR_LOG_FILE="$4"

JOBS="-j16"
REBUILD="-B"

function setup_jack_env()
{
    #sh "${HOME}/cov_scripts/Jack/jack.sh"
    local jack_dir=""${HOME}/cov_scripts/Jack""

    PS=`ps -ef | grep java | grep .jack-server | grep $HOME | wc -l`
    if [ $PS = 0 ];then
        cp -r ${jack_dir}/.jack-server ~/
        cp ${jack_dir}/.jack-settings ~/

        Port1=`cat ${jack_dir}/port1.txt`
        Port2=`cat ${jack_dir}/port2.txt`

        Port1=$(($Port1+2))
        Port2=$(($Port2+2))

        echo "$Port1" > ${jack_dir}/port1.txt
        echo "$Port2" > ${jack_dir}/port2.txt

        sed -i "s/8076/$Port1/g" ~/.jack-server/config.properties 
        sed -i "s/8077/$Port2/g" ~/.jack-server/config.properties 
        sed -i "s/8076/$Port1/g" ~/.jack-settings
        sed -i "s/8077/$Port2/g" ~/.jack-settings

        chmod 700 ~/.jack-server
        chmod 600 ~/.jack-server/client.jks
        chmod 600 ~/.jack-server/client.pem
        chmod 600 ~/.jack-server/launcher.jar
        chmod 600 ~/.jack-server/server-1.jar
        chmod 600 ~/.jack-server/server.jks
        chmod 600 ~/.jack-server/server.pem
        chmod 600 ~/.jack-server/config.properties

        log info "your jack-server port is $Port1 and $Port2"
    fi
}

function setup_android_env()
{
    cd ${PAR_SRC_DIR}
    source build/envsetup.sh

    local board_name=""
    local result=${PAR_PRODUCT%-userdebug*}
    if [ "${result}" =  "${PAR_PRODUCT}" ]; then
        local result1=${PAR_PRODUCT%-user*}
        if [ "${result1}" =  "${PAR_PRODUCT}" ]; then
            log error "${PAR_PRODUCT} isn't a invade board name!"
            return 1
        else
            board_name="${result1}-user"
        fi
    else
        board_name="${result}-userdebug"
    fi

    log info "lunch ${board_name}"
    lunch ${board_name}
}

function do_build()
{
    cd ${PAR_SRC_DIR}
    log info "at $(pwd) build ${PAR_PRODUCT} ${PAR_MODULE} > ${LOG_FILE}"
    if [ "${PAR_MODULE}" = "all" ]; then
        kheader #2>&1 | tee -a ${LOG_FILE}
        log info "make chipram $JOBS"
        make chipram $JOBS 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "chipram ${PAR_PRODUCT} Build Failed!! detail >> ${LOG_FILE}"
            return 1
        fi

        log info "make bootloader $JOBS"
        make bootloader $JOBS 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "bootloader ${PAR_PRODUCT} Build Failed!! detail >> ${LOG_FILE}"
            return 1
        fi

        log info "make bootimage $JOBS"
        make bootimage $JOBS 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "bootimage ${PAR_PRODUCT} Build Failed!! detail >> ${LOG_FILE}"
            return 1
        fi

        log info "make systemimage $JOBS"
        make systemimage $JOBS 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "systemimage ${PAR_PRODUCT} Build Failed!! detail >> ${LOG_FILE}"
            return 1
        fi
    elif [ "$PAR_MODULE" == "kernel" ]; then
        kheader 2>&1 | tee -a ${LOG_FILE}
        log info "make bootimage $JOBS"
        make bootimage $JOBS 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "${PAR_MODULE} Build Failed"
            return 1
        else
            log info "${PAR_PRODUCT}:${PAR_MODULE} build success."
        fi
    elif [ "$PAR_MODULE" == "u-boot" -o "$PAR_MODULE" == "u-boot64" -o "$PAR_MODULE" == "u-boot15" ]; then
        log info "make bootloader $JOBS"
        make bootloader $JOBS 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "${PAR_MODULE} Build Failed"
            return 1
        else
            log info "${PAR_PRODUCT}:${PAR_MODULE} build success."
        fi
    elif [ "$PAR_MODULE" == "chipram" ]; then
        log info "make chipram $JOBS"
        make chipram $JOBS 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "${PAR_MODULE} Build Failed"
            return 1
        else
            log info "${PAR_PRODUCT}:${PAR_MODULE} build success."
        fi
    else
        #cd ${PAR_MODULE}
        local board_dir_name=${PAR_PRODUCT%_*}
        if [ ! -d "${PAR_SRC_DIR}/out/target/product/${board_dir_name}" ];then
            log error "${board_dir_name} not complete compilation, can't build for mmm."
            return 1
        fi

        log info "mmm ${PAR_MODULE} ${REBUILD}"
        mmm ${PAR_MODULE} ${REBUILD} 2>&1 | tee -a ${LOG_FILE}
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            log error "mmm ${PAR_MODULE} Build Failed"
            return 1
        else
            log info "${PAR_MODULE} build finished."
        fi
        cd -
    fi

    return 0
}

function setup_host_env()
{
    local src_base="$1"
#    export USER=covadmin

    local default_platform_version=$(grep -rw "DEFAULT_PLATFORM_VERSION :="  ${src_base}/build/core/version_defaults.mk)
    default_platform_version=${default_platform_version#*:= }

    local grep_word="PLATFORM_VERSION :="
    if [ "" != "${default_platform_version}" ]; then
        grep_word="PLATFORM_VERSION.${default_platform_version} :=" #for adnroid 8.1
    fi

    local platform_version=$(grep -rw "${grep_word}" ${src_base}/build/core/version_defaults.mk)
    platform_version=${platform_version#*:= }
    local first_number=$(echo ${platform_version}|cut -c1)
    case $first_number in
    "4")
        log info "setup JAVA_HOME as jdk1.6.0_29."
        export JAVA_HOME=/usr/java/jdk1.6.0_29
        ;;
    "5")
        log info "setup JAVA_HOME as java-7-openjdk-amd64."
        export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
        ;;
    "6")
        if [ $platform_version} == "6.0.1" ];then
            log info "setup JAVA_HOME as java-8-openjdk-amd64."
            export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
        else
            log info "setup JAVA_HOME as java-7-openjdk-amd64."
            export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
        fi
        ;;
    "7")
        log info "setup JAVA_HOME as java-8-openjdk-amd64."
        setup_jack_env
        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
        ;;
    "8")
        log info "setup JAVA_HOME as java-8-openjdk-amd64."
        setup_jack_env
        REBUILD=""
        export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
        ;;
      *)
        log error "unknown android version at ${src_base} with ${platform_version}, please check your input."
        return 1  
        ;;
    esac

    export PATH=$JAVA_HOME/bin:$JAVA_HOME/jre/bin:$PATH
    export CLASSPATH=$CLASSPATH:.:$JAVA_HOME/lib:$JAVA_HOME/jre/lib
    source /opt/intel/compiler_special_14.0.0.002/bin/compilervars.sh ia32
    ccache -M 50G 2>&1 | tee -a ${LOG_FILE}
    return 0
}

function init_log()
{
    local src_base="$1"
    local log_level="$2"
    local run_date=`date "+%y-%m-%d_%H%M%S"`
    
    if [ ! -d "${PAR_SRC_DIR}/out" ]; then 
      mkdir -p "${PAR_SRC_DIR}/out"
    fi
    if [ "" = "${PAR_LOG_FILE}" ]; then
        LOG_FILE="${PAR_SRC_DIR}/out/build_${run_date}.log"
    else
        LOG_FILE="${PAR_LOG_FILE}"
    fi

    source "${HOME}/cov_scripts/common/log.sh" "${LOG_FILE}" "debug"
}

function main()
{
    if [ "/" = "${PAR_SRC_DIR: -1}" ];then
        PAR_SRC_DIR=${PAR_SRC_DIR%?}
    fi

    if [ ! -d "${PAR_SRC_DIR}" ] || [ "`ls -A ${PAR_SRC_DIR}`" = "" ];then
        log error "The directory ${PAR_SRC_DIR} does not exist or the directory is empty!"
        exit 1
    fi

    init_log "${PAR_SRC_DIR}" "debug"
    log info "=================== build.sh main info ====="
    log info "PAR_SRC_DIR:${PAR_SRC_DIR}"
    log info "PAR_PRODUCT:${PAR_PRODUCT}"
    log info "PAR_MODULE:${PAR_MODULE}"
    log info "PAR_LOG_FILE:${PAR_LOG_FILE}"

    setup_host_env "${PAR_SRC_DIR}"
    if [ $? -ne 0 ]; then
        exit 1
    fi

    setup_android_env
    if [ $? -ne 0 ]; then
        return 1
    fi
    
    do_build
}

main
